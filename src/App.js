import React, {useEffect, useState} from "react";
import axios from 'axios';
import Board from "./Components/Board/Board";
import "./App.css";
import Editable from "./Components/Editabled/Editable";

function App() {

    const [boards, setBoards] = useState([]);

    const getBoardsData = () => {
        axios.get('http://localhost:3003/boards/')
            .then((res) => {
                setBoards(res.data);
            }).catch((err) => {
            console.log(err)
        });
    };

    useEffect(() => {
        getBoardsData();
    }, []);

    const [targetCard, setTargetCard] = useState({
        bid: "",
        cid: "",
    });

    const addboardHandler = (name) => {

        const board = {
            id: Date.now() + Math.random() * 2,
            title: name,
            cards: [],
        };

        axios.post('http://localhost:3003/boards/', board)
            .then((res) => {
                getBoardsData();
            }).catch((err) => {
            console.log(err)
        });
    };

    const removeBoard = (id) => {
        axios.delete(`http://localhost:3003/boards/${id}`)
            .then((res) => {
                getBoardsData();
            }).catch((err) => {
            console.log(err)
        });
    };

    const addCardHandler = (id, title) => {

        const board = boards.find((item) => item.id === id);

        const newBoard = {
            ...board,
            cards: [
                ...board.cards,
                {
                    id: Date.now() + Math.random() * 2,
                    title,
                    labels: [],
                    date: "",
                    tasks: [],
                }
            ]
        };

        axios.put(`http://localhost:3003/boards/${id}`, newBoard)
            .then((res) => {
                getBoardsData();
            }).catch((err) => {
            console.log(err)
        });

    };

    const removeCard = (bid, cid) => {

        const board = boards.find((item) => item.id === bid);
        const Deletedcards = board.cards;

        const cardIndex = Deletedcards.findIndex((item) => item.id === cid);
        if (cardIndex < 0) return;

        Deletedcards.splice(cardIndex, 1);

        const newBoard = {
            ...board,
            cards: [
                ...Deletedcards
            ]
        };

        axios.put(`http://localhost:3003/boards/${bid}`, newBoard)
            .then((res) => {
                getBoardsData();
            }).catch((err) => {
            console.log(err)
        });

    };

    const dragEnded = (bid, cid) => {

        let s_boardIndex, s_cardIndex, t_boardIndex, t_cardIndex;
        s_boardIndex = boards.findIndex((item) => item.id === bid);
        if (s_boardIndex < 0) return;

        s_cardIndex = boards[s_boardIndex]?.cards?.findIndex(
            (item) => item.id === cid
        );

        if (s_cardIndex < 0) return;

        t_boardIndex = boards.findIndex((item) => item.id === targetCard.bid);
        if (t_boardIndex < 0) return;

        t_cardIndex = boards[t_boardIndex]?.cards?.findIndex(
            (item) => item.id === targetCard.cid
        );
        if (t_cardIndex < 0) return;

        const tempBoards = [...boards];
        const sourceCard = tempBoards[s_boardIndex].cards[s_cardIndex];
        tempBoards[s_boardIndex].cards.splice(s_cardIndex, 1);
        tempBoards[t_boardIndex].cards.splice(t_cardIndex, 0, sourceCard);
        setBoards(tempBoards);

        setTargetCard({
            bid: "",
            cid: "",
        });
    };


    const dragEntered = (bid, cid) => {
        if (targetCard.cid === cid) return;
        setTargetCard({
            bid,
            cid,
        });
    };

    const updateCard = (bid, cid, card) => {

        const board = boards.find((item) => item.id === bid);
        const UpdatedCards = board.cards;

        const cardIndex = UpdatedCards.findIndex((item) => item.id === cid);
        if (cardIndex < 0) return;

        board.cards[cardIndex] = card;

        const newBoard = {
            ...board,
            cards: [
                ...UpdatedCards
            ]
        };

        axios.put(`http://localhost:3003/boards/${bid}`, newBoard)
            .then((res) => {
                getBoardsData();
            }).catch((err) => {
            console.log(err)
        });
    };

    return (
        <div className="app">
            <div className="app_nav">
                <h1>Kanban Board</h1>
            </div>
            <div className="app_boards_container">
                <div className="app_boards">
                    {boards.map((item) => (
                        <Board
                            key={item.id}
                            board={item}
                            addCard={addCardHandler}
                            removeBoard={() => removeBoard(item.id)}
                            removeCard={removeCard}
                            dragEnded={dragEnded}
                            dragEntered={dragEntered}
                            updateCard={updateCard}
                        />
                    ))}
                    <div className="app_boards_last">
                        <Editable
                            displayClass="app_boards_add-board"
                            editClass="app_boards_add-board_edit"
                            placeholder="Enter Board Name"
                            text="Add Board"
                            buttonText="Add Board"
                            onSubmit={addboardHandler}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
